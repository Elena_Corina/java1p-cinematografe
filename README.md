Aplicatie pentru un cinematografe

Definim entitatea cinematograf, despre care cunoastem urmatoarele informatii:
* adresa
* numar_sali

Definim entitatea Film cu urmatoarele atribute:
* gen - genul poate sa fie Comedie sau Drama
* titlul
* durata
* limba_dublare - daca este cazul

Stim ca un film poate rula in acelasi timp in mai multe cinematografe, iar un cinematograf poate rula mai multe filme. Legatura dintre cele doua entitati se va face cu ajutorul unei entitati Bilet.

Aplicatia are urmatoarele functionalitati:
* adaugare_cinematograf
* adaugare_film 
* adaugare_bilet
* afisare_cinematografe
* afisare_filme
* afisare_comedii
* exit 

Pentru a executa aceste functionalitati, aplicatia va citi comenzile din linie de comanda. Citirea parametrilor pentru o comanda se va face  pe linii diferite
ex: 
adaugare_film
Titlu: Titanic
Gen: Drama
Durata: 180

Adaugarea de inregistrari in entitatea Bilet se face prin specificarea ID-urilor filmului pentru care biletul este emis si a cinematografului in care se face rularea.

Datele legate de filme si cinematografe vor fi salvate intr-o baza de date, pe un server ce va rula intr-o masina virtuala de Linux.
Serviciul de MySQL trebuie configurat sa porneasca in mod automat, in momentul in care este pornita si masina virtuala.

Bonus: Adresa IP a serverului de baze de date, impreuna cu credentialele de autentificare vor fi citite ca si argumente in momentul rularii aplicatiei de mai sus.
Bonus 2: La adaugarea de bilet, adaugarea se poate face prin specificarea numelui cinematografului si a filmului. (HINT: Trebuie preluate ID-urile pentru numele filmului si a cinematografului si abia dupa facut insert-ul.)

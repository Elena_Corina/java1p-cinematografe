package model;

public class Bilet {
    private int id_cinematograf;
    private int id_film;

    public Bilet() {
    }

    public Bilet(int id_cinematograf, int id_film) {
        this.id_cinematograf = id_cinematograf;
        this.id_film = id_film;
    }

    public int getId_cinematograf() {
        return id_cinematograf;
    }

    public void setId_cinematograf(int id_cinematograf) {
        this.id_cinematograf = id_cinematograf;
    }

    public int getId_film() {
        return id_film;
    }

    public void setId_film(int id_film) {
        this.id_film = id_film;
    }
}

package model;

public class Film {
    private int id;
    private String gen;
    private String titlul;
    private int durata;
    private String limba_dublare;

    public Film() {
    }

    public Film(String gen, String titlul, int durata, String limba_dublare) {
        this.gen = gen;
        this.titlul = titlul;
        this.durata = durata;
        this.limba_dublare = limba_dublare;
    }

    public Film (int id, String gen, String titlul, int durata, String limba_dublare) {
        this.id = id;
        this.gen = gen;
        this.titlul = titlul;
        this.durata = durata;
        this.limba_dublare = limba_dublare;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public String getTitlul() {
        return titlul;
    }

    public void setTitlul(String titlul) {
        this.titlul = titlul;
    }

    public int getDurata() {
        return durata;
    }

    public void setDurata(int durata) {
        this.durata = durata;
    }

    public String getLimba_dublare() {
        return limba_dublare;
    }

    public void setLimba_dublare(String limba_dublare) {
        this.limba_dublare = limba_dublare;
    }

    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", gen='" + gen + '\'' +
                ", titlul='" + titlul + '\'' +
                ", durata=" + durata +
                ", limba_dublare='" + limba_dublare + '\'' +
                '}';
    }
}

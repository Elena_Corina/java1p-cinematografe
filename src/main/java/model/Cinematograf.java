package model;

import java.util.Objects;

public class Cinematograf {

    private int id;
    private String adresa;
    private int numar_sali;

    public Cinematograf() {
    }

    public Cinematograf(String adresa, int numar_sali) {
        this.adresa = adresa;
        this.numar_sali = numar_sali;
    }

    public Cinematograf(int id, String adresa, int numar_sali) {
        this.id = id;
        this.adresa = adresa;
        this.numar_sali = numar_sali;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public int getNumar_sali() {
        return numar_sali;
    }

    public void setNumar_sali(int numar_sali) {
        this.numar_sali = numar_sali;
    }

    @Override
    public String toString() {
        return "Cinematograf{" +
                "id=" + id +
                ", adresa='" + adresa + '\'' +
                ", numar_sali=" + numar_sali +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cinematograf that = (Cinematograf) o;
        return id == that.id && numar_sali == that.numar_sali && Objects.equals(adresa, that.adresa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, adresa, numar_sali);
    }
}

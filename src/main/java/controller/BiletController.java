package controller;

import dao.BiletDao;
import model.Bilet;

import java.sql.SQLException;
import java.util.Optional;

public class BiletController {
    private BiletDao biletDao;

    private static final class SingletonHolder{
        private static final BiletController INSTANCE = new BiletController();
    }

    private BiletController(){
        try {
            biletDao = new BiletDao(ConnectionManager.getInstance().getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static BiletController getInstance(){
       return SingletonHolder.INSTANCE;
    }

    public boolean adaugare_bilet(Bilet bilet) throws SQLException {
        BiletDao biletDao = new BiletDao(ConnectionManager.getInstance().getConnection());
        boolean rez = false;
        try {
            Optional<Bilet> optionalBilet = biletDao.findBilet(bilet.getId_cinematograf(), bilet.getId_film());
            if (!optionalBilet.isPresent()) {
                biletDao.adaugaBilet(bilet);
                rez = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rez;
    }

}

package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class ConnectionManager {

    private Connection con;

    private static final class SingletonHolder{
        public static final ConnectionManager INSTANCE = new ConnectionManager();
    }

    private ConnectionManager( ){

        String url = "jdbc:mysql://172.20.20.201:3306/app_cinematografe";
        String user = "root2";
        String pass ="12345678";

        try {
            con = DriverManager.getConnection(url, user, pass);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ConnectionManager getInstance(){
        return SingletonHolder.INSTANCE;
    }

    public Connection getConnection(){
        return con;
    }


}

package controller;

import dao.CinematografDao;
import dao.FilmDao;
import model.Cinematograf;
import model.Film;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class FilmController {

    private FilmDao filmDao;

    private static final class SingletonHolder{
        private static final FilmController INSTANCE = new FilmController();
    }

    private FilmController(){
        try {
            filmDao = new FilmDao(ConnectionManager.getInstance().getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static FilmController getInstance(){
        return SingletonHolder.INSTANCE;
    }


    public boolean adaugare_film(Film film) throws SQLException {
        FilmDao filmDao = new FilmDao(ConnectionManager.getInstance().getConnection());
        boolean rez = false;
        try {
            Optional<Film> optionalFilm = filmDao.findFilm(film.getTitlul());
            if(!optionalFilm.isPresent()){
                filmDao.adaugaFilm(film);
                rez = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rez;
    }

    public List<Film> afisare_filme() throws SQLException {
        FilmDao filmDao = new FilmDao(ConnectionManager.getInstance().getConnection());
        try {
            return filmDao.afisareFilm();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public List<Film> afisare_comedii() throws SQLException {
        FilmDao filmDao = new FilmDao(ConnectionManager.getInstance().getConnection());
        try {
            return filmDao.afisareComedii();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public int getIdFilm (String titlul) throws SQLException {
        FilmDao filmDao = new FilmDao(ConnectionManager.getInstance().getConnection());
        return filmDao.getIdFilm(titlul);
    }

}

package controller;

import dao.CinematografDao;
import model.Cinematograf;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class CinematografController {

    private CinematografDao cinematografDao;

    private static final class SingletonHolder{
        public static final CinematografController INSTANCE = new CinematografController();
    }

    private CinematografController(){
        cinematografDao  = new CinematografDao(
                ConnectionManager.getInstance().getConnection());
    }


    public static CinematografController getInstance(){
        return SingletonHolder.INSTANCE;
    }


    public boolean adaugare_cinematograf(Cinematograf cinematograf){
        CinematografDao cinematografDao = new CinematografDao(ConnectionManager.getInstance().getConnection());
        boolean rez = false;
        try {
            Optional<Cinematograf> optionalCinematograf = cinematografDao.findCinematograf(cinematograf.getAdresa());
            if(!optionalCinematograf.isPresent()){
                cinematografDao.adaugaCinematograf(cinematograf);
                rez = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rez;
    }

    public List<Cinematograf> afisare_cinematografe(){
        CinematografDao cinematografDao = new CinematografDao(ConnectionManager.getInstance().getConnection());
        try {
            return  cinematografDao.getAllCinema();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public int getIdCinematograf (String adresa) throws SQLException {
        CinematografDao cinematografDao = new CinematografDao(ConnectionManager.getInstance().getConnection());
        return cinematografDao.getIdCinematograf(adresa);
    }
}

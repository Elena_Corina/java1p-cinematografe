package view;

import controller.FilmController;
import model.Cinematograf;
import model.Film;

import javax.swing.*;
import java.sql.SQLException;
import java.util.List;

public class FilmFrame extends JFrame{
    private JList<model.Film> listaFilme;
    private JButton adaugaFilmButton;
    private JButton afiseazaFilmeButton;
    private JButton afiseazaComediiButton;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JPanel panel1;
    private JPanel panel2;
    private JPanel mainPanel;
    private JLabel Durata;
    private JFrame filmFrame;
    private DefaultListModel<Film> model;

    public FilmFrame(){
        JFrame filmFrame = new JFrame("FILM");
        filmFrame.add(mainPanel);

        model = new DefaultListModel<>();
        listaFilme.setModel(model);

        adaugaFilmButton.addActionListener(ev -> adaugare_film());
        afiseazaFilmeButton.addActionListener(ev -> afisare_filme());
        afiseazaComediiButton.addActionListener(ev -> afisare_comedii());

        filmFrame.setSize(700,500);
        filmFrame.setLocationRelativeTo(null);
        filmFrame.setVisible(true);
    }

    private void adaugare_film(){
        String gen = textField1.getText();
        String titlu = textField2.getText();
        int durata = Integer.parseInt(textField3.getText());
        String limba_dublare = textField4.getText();

        Film film = new Film(gen, titlu, durata, limba_dublare);

        try {
            if(FilmController.getInstance().adaugare_film(film)){
                afisare_filme();
                JOptionPane.showMessageDialog(null, "Filmul a fost adaugat");
            } else {
                JOptionPane.showMessageDialog(null, "Filmul nu a fost adaugat");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void afisare_filme() {
        List<Film> lista_filme = null;
        try {
            lista_filme = FilmController.getInstance().afisare_filme();
            model.clear();
            lista_filme.forEach(model::addElement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void afisare_comedii(){
        List<Film> lista_filme = null;
        try {
            lista_filme = FilmController.getInstance().afisare_comedii();
            model.clear();
            lista_filme.forEach(model::addElement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

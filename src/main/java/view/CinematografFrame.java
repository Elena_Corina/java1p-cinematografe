package view;

import controller.CinematografController;
import model.Cinematograf;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.util.List;

public class CinematografFrame  extends JFrame{
    private JPanel panel1;
    private JList<model.Cinematograf> lista_cinema;
    private DefaultListModel<Cinematograf> model;
    private JTextField textField1;
    private JTextField textField2;
    private JButton add_cinema_Button;
    private JPanel panelCinema;
    private JLabel adresa;
    private JLabel nr_sali;

    public CinematografFrame(){
        JFrame cinematografFrame = new JFrame("CINEMATOGRAF");
        cinematografFrame.add(panel1);

        model = new DefaultListModel<>();
        lista_cinema.setModel(model);

        add_cinema_Button.addActionListener(ev -> adaugare_cinematograf());
        afisare_cinematografe();

       lista_cinema.addMouseListener(new MouseAdapter() {
                                         @Override
                                         public void mouseClicked(MouseEvent ev) {
                                             if (ev.getClickCount() == 2) {
                                                 Cinematograf cinematograf = lista_cinema.getSelectedValue();
                                                 new FilmFrame();
                                                cinematografFrame.dispose();
                                             }
                                         }
                                     });


        cinematografFrame.setSize(700,500);
        cinematografFrame.setLocationRelativeTo(null);
        cinematografFrame.setVisible(true);
    }

    private void adaugare_cinematograf(){
        String adresa = textField1.getText();
        int nr_sali = Integer.parseInt(textField2.getText());
        Cinematograf cinema = new Cinematograf(adresa, nr_sali);

        if(CinematografController.getInstance().adaugare_cinematograf(cinema)){
            afisare_cinematografe();
            JOptionPane.showMessageDialog(null,"Cinematograful a fost adaugat");
        } else {
            JOptionPane.showMessageDialog(null,"Cinematograful nu a fost adaugat");
        }

    }

    private void afisare_cinematografe(){
        List<Cinematograf> cinema = CinematografController.getInstance().afisare_cinematografe();
        model.clear();
        cinema.forEach(model::addElement);
    }



}

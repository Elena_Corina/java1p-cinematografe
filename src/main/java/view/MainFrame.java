package view;

import javax.swing.*;

public class MainFrame {
    private JButton FILMButton;
    private JButton BILETButton;
    private JButton CINEMATOGRAFButton;
    private JComboBox comboBox1;
    private JPanel mainPanel;
    private JButton loginButton;
    private JButton exitButton;
    private JFrame mainFrame;

    public MainFrame(){
       JFrame mainFrame = new JFrame("Aplicatie Cinematografe");
       mainFrame.add(mainPanel);
       loginButton.addActionListener(ev -> login());
       exitButton.addActionListener(ev -> exit());
       CINEMATOGRAFButton.addActionListener(ev-> openCinematograf());
       FILMButton.addActionListener(ev -> openFilme());
       BILETButton.addActionListener(ev -> openBilet());

       mainFrame.setSize(200,200);
       mainFrame.setLocationRelativeTo(null);
       mainFrame.setVisible(true);
    }

    private void login(){
        new CinematografFrame();
    }

    private void exit() {
       System.exit(0);
    }

    private void openCinematograf(){
        new CinematografFrame();
    }

    private void openFilme(){
        new FilmFrame();
    }
    private void openBilet(){
        new BiletFrame();
    }
}

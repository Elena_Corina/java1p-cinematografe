package view;

import controller.BiletController;
import controller.CinematografController;
import controller.FilmController;
import dao.BiletDao;
import model.Bilet;

import javax.swing.*;
import java.sql.SQLException;

public class BiletFrame extends JFrame{
    private JPanel panel1;
    private JTextField adresaTextField;
    private JTextField titlulTextField;
    private JButton adaugaBiletButton;
    private JFrame biletFrame;

    public BiletFrame(){
        JFrame biletFrame = new JFrame("BILET");
        biletFrame.add(panel1);

        adaugaBiletButton.addActionListener(ev -> adaugare_bilet());

        biletFrame.setSize(500,500);
        biletFrame.setLocationRelativeTo(null);
        biletFrame.setVisible(true);
    }

    public void adaugare_bilet()  {
        CinematografController cinemaC = CinematografController.getInstance();
        int  id_cinematograf = 0;
        try {
            id_cinematograf = cinemaC.getIdCinematograf(adresaTextField.getText());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        FilmController filmC = FilmController.getInstance();
        int id_film = 0;
        try {
            id_film = filmC.getIdFilm(titlulTextField.getText());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Bilet bilet = new Bilet(id_cinematograf, id_film);

        try {
            if(BiletController.getInstance().adaugare_bilet(bilet)){
                JOptionPane.showMessageDialog(null, "Biletul a fost adaugat");
            }else{
                JOptionPane.showMessageDialog(null, "Biletul nu a fost adaugat");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

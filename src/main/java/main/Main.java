package main;

import dao.CinematografDao;
import model.Cinematograf;
import view.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        new MainFrame();
    }
}

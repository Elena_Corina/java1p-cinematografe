package dao;

import model.Bilet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class BiletDao {

    private Connection con;
    private PreparedStatement stmt1, stmt2;

    public BiletDao(Connection con) throws SQLException {
        this.con = con;
        this.stmt1 = con.prepareStatement("INSERT INTO bilet VALUES(?,?)");
        this.stmt2 = con.prepareStatement("SELECT * FROM bilet WHERE id_cinematograf = ? AND id_film =?");
    }

    public void adaugaBilet(Bilet bilet) throws SQLException {
       stmt1.setInt(1, bilet.getId_cinematograf());
       stmt1.setInt(2, bilet.getId_film());
       stmt1.executeUpdate();
    }

    public Optional<Bilet> findBilet(int id_cinema, int id_film) throws SQLException {
        stmt2.setInt(1, id_cinema);
        stmt2.setInt(2, id_film);
        Bilet bilet = null;

        try(ResultSet rs = stmt2.executeQuery()){
            if(rs.next()) {
                bilet = new Bilet();
                bilet.setId_cinematograf(rs.getInt("id_cinematograf"));
                bilet.setId_film(rs.getInt("id_film"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return  Optional.ofNullable(bilet);
    }
}

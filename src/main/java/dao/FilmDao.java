package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import model.Film;

public class FilmDao {
    private Connection con;
    private PreparedStatement stmt1, stmt2, stmt3, stmt4, stmt5;

    public FilmDao(Connection con) throws SQLException {
        try {
            this.con =con;
            this.stmt1 = con.prepareStatement("INSERT INTO film VALUES (NULL,?,?,?,?)");
            this.stmt2 = con.prepareStatement("SELECT * FROM film WHERE titlul = ?");
            this.stmt3 = con.prepareStatement("SELECT * FROM film");
            this.stmt4 = con.prepareStatement("SELECT * FROM film WHERE gen = ?");
            this.stmt5 = con.prepareStatement("SELECT id FROM film WHERE titlul = ?");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void adaugaFilm (Film film) throws SQLException {
        stmt1.setString(1, film.getGen());
        stmt1.setString(2, film.getTitlul());
        stmt1.setInt(3, film.getDurata());
        stmt1.setString(4, film.getLimba_dublare());
        stmt1.executeUpdate();
    }

    public Optional<Film> findFilm(String titlul) throws SQLException {
        stmt2.setString(1, titlul);
        Film f = null;
        try(ResultSet rs = stmt2.executeQuery()){
            if(rs.next()){
                f = new Film();
                f.setId(rs.getInt("id"));
                f.setGen(rs.getString("gen"));
                f.setTitlul(rs.getString("titlul"));
                f.setDurata(rs.getInt("durata"));
                f.setLimba_dublare(rs.getString("limba_dublare"));
            }
        }
        return Optional.ofNullable(f);

    }

    public List<Film> afisareFilm() throws SQLException {
        List<Film> filme = new ArrayList<>();
        ResultSet rs = stmt3.executeQuery();

        while(rs.next()){
            int id = rs.getInt("id");
            String gen = rs.getString("gen");
            String titlul = rs.getString("titlul");
            int durata =rs.getInt("durata");
            String dublare = rs.getString("limba_dublare");

            Film film = new Film(id, gen, titlul, durata, dublare);
            filme.add(film);
        }
        return filme;
    }

    public List<Film> afisareComedii() throws SQLException {
        List<Film> comedii = new ArrayList<>();
        stmt4.setString(1, "Comedie");
        ResultSet rs = stmt4.executeQuery();
        while(rs.next()){
            int id = rs.getInt("id");
            String gen = rs.getString("gen");
            String titlul = rs.getString("titlul");
            int durata = rs.getInt("durata");
            String dublare = rs.getString("limba_dublare");

            Film comedie = new Film(id, gen, titlul, durata, dublare);
            comedii.add(comedie);
        }
        return comedii;
    }

    public int getIdFilm(String titlul) throws SQLException {
        int id_film = 0;
        stmt5.setString(1, titlul);
        ResultSet rs = stmt5.executeQuery();
        rs.next();
        id_film= rs.getInt(1);
        return id_film;
    }
}

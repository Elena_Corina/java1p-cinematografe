package dao;

import controller.ConnectionManager;
import model.Cinematograf;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

public class CinematografDao {
    private Connection con;
    private PreparedStatement stmt1, stmt2, stmt3, stmt4;

    public CinematografDao(Connection con){
        try {
            this.con = con;
            this.stmt1 = con.prepareStatement("INSERT INTO cinematograf VALUES(NULL, ?,?)");
            this.stmt2 = con.prepareStatement("SELECT * FROM cinematograf WHERE adresa =? ");
            this.stmt3 = con.prepareStatement("SELECT * FROM cinematograf");
            this.stmt4 = con.prepareStatement("SELECT id FROM cinematograf WHERE adresa = ?");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void adaugaCinematograf(Cinematograf cinematograf) throws SQLException {
        stmt1.setString(1, cinematograf.getAdresa());
        stmt1.setInt(2, cinematograf.getNumar_sali());
        stmt1.executeUpdate();
    }

    public Optional<Cinematograf> findCinematograf(String adresa) throws SQLException {
            stmt2.setString(1, adresa);
            Cinematograf cinematograf = null;
            try(ResultSet rs = stmt2.executeQuery()){
                if(rs.next()){
                    cinematograf = new Cinematograf();
                    cinematograf.setId(rs.getInt("id"));
                    cinematograf.setAdresa(rs.getString("adresa"));
                    cinematograf.setNumar_sali(rs.getInt("numar_sali"));
                }
            }
            return Optional.ofNullable(cinematograf);
    }

    public List<Cinematograf> getAllCinema() throws SQLException {
        List<Cinematograf> cinema = new ArrayList<>();
        ResultSet rs = stmt3.executeQuery();

        while(rs.next()){
            int id = rs.getInt("id");
            String adresa = rs.getString("adresa");
            int nr_sali = rs.getInt( "numar_sali");

            cinema.add(new Cinematograf(id, adresa, nr_sali));
        }
        return cinema;
    }

    public int getIdCinematograf(String adresa) throws SQLException {
        int id_cinematograf = 0;
        stmt4.setString(1, adresa);
        ResultSet rs = stmt4.executeQuery();
        rs.next();
        id_cinematograf = rs.getInt(1);
        return id_cinematograf;
    }
}


